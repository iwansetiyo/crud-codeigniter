<?php
class Users extends CI_Controller{
  function __construct(){
    parent::__construct();

    if($this->session->userdata('token') == ''){
      redirect('login');
    }

    $this->load->model('users_model');

  }
 
  function index(){

    $data['title'] = 'Users';

    $json_users         = $this->users_model->get_all();
    $data['list_users'] = json_decode($json_users);

    $this->load->view('site/header', $data);
    $this->load->view('users/user_index', $data);
    $this->load->view('site/footer');
  }

  function add(){
    $data['title'] = 'Add User';

    $this->load->view('site/header', $data);
    $this->load->view('users/user_add', $data);
    $this->load->view('site/footer');
  }

  function edit($id=0){
    if($id > 0){
      $data['title'] = 'Edit User';

      $data['id']         = $id;
      $json_user          = $this->users_model->get_detail($id);
      $data['data_user']  = json_decode($json_user);

      $this->load->view('site/header', $data);
      $this->load->view('users/user_edit', $data);
      $this->load->view('site/footer');
    }
  }

  function save_new(){
    if($this->input->post()){
      $email      = $this->input->post('user_email');
      $first_name = $this->input->post('user_first_name');
      $last_name  = $this->input->post('user_last_name');

      $array_user = array(
        'email' => $email,
        'first_name' => $first_name,
        'last_name' => $last_name
      );

      $user_id = $this->users_model->save_new($array_user);

      redirect('users');
    }
  }

  function update($id=0){
    if( $id > 0 ){
      $email      = $this->input->post('user_email');
      $first_name = $this->input->post('user_first_name');
      $last_name  = $this->input->post('user_last_name');

      $array_user = array(
        'email'       => $email,
        'first_name'  => $first_name,
        'last_name'   => $last_name
      );

      $user_id = $this->users_model->update($id, $array_user);

      redirect('users');
    }
  }

  function view($id=0){
    $data['title'] = 'Detail User';

    if($id > 0){
      $json_user         = $this->users_model->get_detail($id);
      $data['data_user']  = json_decode($json_user);

      $this->load->view('site/header', $data);
      $this->load->view('users/user_view', $data);
      $this->load->view('site/footer');
    }
  }

  function delete($id=0){
    if($id > 0) {
      $user_id = $this->users_model->delete($id);
      redirect('users');
    }
  }
 
}