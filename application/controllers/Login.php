<?php
class Login extends CI_Controller{
	function __construct(){
		parent::__construct();

		$this->load->model('login_model');
	}

	function index(){
		if($this->session->userdata('logged_in')){
			redirect('page');
	    }

		$this->load->view('login/login_index');
	}

	function auth(){
		if($this->session->userdata('logged_in')){
			redirect('page');
	    }

		$email 		= $this->input->post('email');
		$password 	= $this->input->post('password');

		$login 		= array( 
			"email" => $email, 
			"password" => $password 
		);
		$user 		= $this->login_model->validate($login);
		$array_user = json_decode($user);

		if(isset($array_user->token)){
			$sesdata = array(
				'token' => $array_user->token,
				'logged_in' => TRUE
			);
			$this->session->set_userdata($sesdata);
			redirect('page');
		}else{
			echo $this->session->set_flashdata('msg','Username or Password is Wrong');
			redirect('login');
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}

	function register()
	{
		$data = array();
		$this->load->view('login/register', $data);
	}

	function register_thanks()
	{
		$this->load->view('login/register_thanks');
	}

	function save_new()
	{
		if($this->input->post())
		{
			$set_array_user = array(
				'email' => $this->input->post('user_email'),
				'password' => $this->input->post('user_password'),
			);

			$user = $this->login_model->register($set_array_user);


			redirect('login/register_thanks');
		}
	}

}