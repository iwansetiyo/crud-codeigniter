<?php
class Page extends CI_Controller{
  function __construct(){
    parent::__construct();
  }
 
  function index(){
    if($this->session->userdata('token') == ''){
      redirect('login');
    }

    $data['title'] = 'Dashboard';

    $this->load->view('site/header', $data);
    $this->load->view('dashboard/dashboard_index');
    $this->load->view('site/footer');
  }
 
}