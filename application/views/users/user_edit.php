<div class="col-lg-12">
    <div class="panel-body">
        <form action="<?php echo site_url('users/update/'.$id);?>" method="POST">
            <div class="row">
                <div class="form-group col-lg-12">
                    <label>Email</label>
                    <input type="text" class="form-control" name="user_email" value="<?php echo $data_user->data->email; ?>">
                </div>
                <div class="form-group col-lg-12">
                    <label>First Name</label>
                    <input type="text" class="form-control" name="user_first_name" value="<?php echo $data_user->data->first_name; ?>">
                </div>
                <div class="form-group col-lg-12">
                    <label>Last Name</label>
                    <input type="text" class="form-control" name="user_last_name" value="<?php echo $data_user->data->last_name; ?>">
                </div>
            </div>
            <button type="submit" class="btn btn-success">Save</button>
            <a href="<?php echo base_url(); ?>users" class=" back-button btn btn-primary">Back</a>
        </form>
    </div>
</div>