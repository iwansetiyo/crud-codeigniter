<div class="col-lg-12">
    <div class="panel-body">
      <a href="<?php echo base_url()?>users/add" class="btn btn-primary">Add New</a>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Email</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php if(count($list_users)){ 
            $n = 1;
            ?>
            <?php foreach ($list_users->data as $data_user){ ?>
              <tr>
                <th scope="row"><?php echo $n; ?></th>
                <td><?php echo $data_user->email; ?></td>
                <td><?php echo $data_user->first_name; ?></td>
                <td><?php echo $data_user->last_name; ?></td>
                <td>
                  <a href="<?php echo base_url()?>users/view/<?php echo $data_user->id; ?>"><i class="fa fa-eye"></i></a>
                  <a href="<?php echo base_url()?>users/edit/<?php echo $data_user->id; ?>"><i class="fa fa-pencil"></i></a>
                  <a href="<?php echo base_url()?>users/delete/<?php echo $data_user->id; ?>"><i class="fa fa-trash"></i></a>
                </td>
              </tr>
            <?php $n++ ; } ?>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>