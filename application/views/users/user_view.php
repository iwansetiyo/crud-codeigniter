<div class="panel panel-default">
  <div class="panel-body">
    <div class="row">
    	<div class="col-md-2"> Email </div>
    	<div class="col-md-1">:</div>
    	<div class="col-md-9"> <?php echo $data_user->data->email; ?> </div>
    </div>
    <div class="row">
    	<div class="col-md-2"> First Name </div>
    	<div class="col-md-1">:</div>
    	<div class="col-md-9"> <?php echo $data_user->data->first_name; ?> </div>
    </div>
    <div class="row">
    	<div class="col-md-2"> Last Name </div>
    	<div class="col-md-1">:</div>
    	<div class="col-md-9"> <?php echo $data_user->data->last_name; ?> </div>
    </div>
  </div>
</div>