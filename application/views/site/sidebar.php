<nav id="sidebar" class="">
    <div class="sidebar-header">
    </div>
    <div class="left-custom-menu-adp-wrap comment-scrollbar">
        <nav class="sidebar-nav left-sidebar-menu-pro">
            <ul class="metismenu" id="menu1">
                <li <?php echo ($this->router->class == 'page') ? 'active' : '';?>>
                    <a title="Dashboard" href="<?php echo base_url(); ?>" aria-expanded="false"><span class="fa fa-home" aria-hidden="true"></span> <span class="mini-click-non">Dashboard</span></a>
                </li>
                <li <?php echo ($this->router->class == 'page') ? 'active' : '';?>>
                    <a title="User" href="<?php echo base_url(); ?>users" aria-expanded="false"><span class="fa fa-users" aria-hidden="true"></span> <span class="mini-click-non">User</span></a>
                </li>
            </ul>
        </nav>
    </div>
</nav>