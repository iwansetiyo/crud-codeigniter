<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Login</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Play:400,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css">
</head>

<body class="login-body">
  <div class="error-pagewrap">
    <div class="error-page-int">
      <div class="text-center m-b-md custom-login">
      </div>
      <div class="content-error">
        <div class="hpanel">
            <div class="panel-body">
                <h3 class="text-center">Silahkan Login</h3>
                <hr>
                <form action="<?php echo site_url('login/auth');?>" method="post" id="loginForm">
                    <div class="form-group">
                        <label class="control-label" for="username">Username</label>
                        <input type="email" placeholder="example@gmail.com" title="Please enter you username" required="" value="" name="email" id="username" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="password">Password</label>
                        <input type="password" title="Please enter your password" placeholder="******" required="" value="" name="password" id="password" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-success btn-block loginbtn">Login</button>
                </form>
            </div>
        </div>
      </div>
      <div class="text-center login-footer">
      </div>
    </div>   
  </div>
  <script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.12.4.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
</body>

</html>