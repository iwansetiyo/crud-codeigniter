<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Register</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Play:400,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/style.css">
</head>        
<body class="register-body">
    <div class="error-pagewrap">
        <div class="error-page-int">
            <div class="text-center custom-login">
    				<h3>Registration</h3>
    			</div>
    			<div class="content-error">
    				<div class="hpanel">
                        <div class="panel-body">
                            <form action="<?php echo site_url('login/save_new');?>" method="POST" id="loginForm">
                                <div class="row">
                                    <div class="form-group col-lg-12">
                                        <label>Email</label>
                                        <input class="form-control" name="user_email">
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <label>Password</label>
                                        <input type="password" class="form-control" name="user_password">
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success loginbtn">Register</button>
                                    <button class="btn btn-default">Cancel</button>
                                </div>
                                <p class="text-center">Already have account? Login <a href="<?php echo base_url(); ?>login" style="background: #fff; color:#006DF0;">here</a> </p>
                            </form>
                        </div>
                    </div>
    			</div>
            </div>
        </div>
  
  <script src="<?php echo base_url(); ?>assets/js/vendor/jquery-1.12.4.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
</body>

</html>