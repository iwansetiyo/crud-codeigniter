<?php
class Login_model extends CI_Model{

	function validate($login=array()){

		$headers 	= array('Content-Type: application/json',);
		$url 		= 'https://reqres.in/api/login';

		$postfields = json_encode($login);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$contents = curl_exec($ch);

		return $contents;
	}

	function register($set_array=array())
	{
		$headers 	= array('Content-Type: application/json',);
		$url 		= 'https://reqres.in/api/register';

		$postfields = json_encode($set_array);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$contents = curl_exec($ch);

		return $contents;
	}
 
}