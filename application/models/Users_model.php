<?php
class Users_model extends CI_Model{

	function get_all(){

		$headers 	= array('Content-Type: application/json',);
		$url 		= 'https://reqres.in/api/users';

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$contents = curl_exec($ch);

		return $contents;
	}

	function get_detail($id=0){

		$headers 	= array('Content-Type: application/json',);
		$url 		= 'https://reqres.in/api/users/'.$id;

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$contents = curl_exec($ch);

		return $contents;
	}

	function save_new($set_array = array()) {
		$headers 	= array('Content-Type: application/json',);
		$url 		= 'https://reqres.in/api/users';

		$postfields = json_encode($set_array);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$contents = curl_exec($ch);

		return $contents;
	}

	function update($id=0, $set_array = array()) {
		$headers 	= array('Content-Type: application/json',);
		$url 		= 'https://reqres.in/api/users/'.$id;

		$postfields = json_encode($set_array);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$contents = curl_exec($ch);

		return $contents;
	}

	function delete($id=0) {
		$headers 	= array('Content-Type: application/json',);
		$url 		= 'https://reqres.in/api/users/'.$id;

		$postfields = json_encode($set_array);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$contents = curl_exec($ch);

		return $contents;
	}
 
}